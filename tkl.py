# -*- coding: utf-8 -*-
"""
Created on Mon May 23 15:00:25 2022

@author: Vladimir Bruaner
"""

#Importing libraries
import numpy as np
from PIL import Image

import matplotlib.pyplot as plt
import math 
import os




def fromPicToMatrix(picturePath):
    "Transforme une image en matrice de niveaux de gris"
    
    img = Image.open(picturePath)
    imgGray = img.convert('L')
    imgArray = np.array(imgGray)
    
    return(imgArray)


def fromMatrixToPic(imgMatrix):
    "Transforme une matrice en image"
    
    pil_image=Image.fromarray(imgMatrix)
    pil_image.show()
    

def adaptedMatrix(initialMatrix, blockSize):
    "Retourne une matrice adaptée à la taille des blocks"
    a = initialMatrix.shape
    width = a[1]
    height = a[0]
    newWidth = 0
    newHeight = 0
    
    if((width % blockSize) != 0):
        #La largeure de l'image ne tombe pas juste avec notre taille de blocks, il faut ajouter des 0
        newWidth = width + (blockSize - (width % blockSize))
    else:
        newWidth = width

    if((height % blockSize) != 0):
        #La largeure de l'image ne tombe pas juste avec notre taille de blocks, il faut ajouter des 0
        newHeight = height + (blockSize - (height % blockSize))
    else:
        newHeight = height

    if(newWidth != 0 or newHeight != 0):
        newMatrix = np.zeros((newHeight, newWidth))
        for i in range(0,height):
            for j in range(0,width):
                newMatrix[i][j] = initialMatrix[i][j]
        
        return(newMatrix)
    
    else:
        return(initialMatrix)
    

    
def vectorise(matrix, blockSize):
    "Retourne une liste de vecteurs correspondants à chaque block de l'image"
    matrice = adaptedMatrix(matrix, blockSize)
    a = matrice.shape
    vectorList = []
    
    for i in range(0, a[0], blockSize):
        for j in range(0, a[1], blockSize):
            
            vector = np.zeros((blockSize*blockSize,1))
            iterator = 0
            
            for k in range(i, i+blockSize):
                for l in range(j, j+blockSize):
                    
                    vector[iterator][0] = matrice[k][l]
                    iterator = iterator + 1
                    
            
            vectorList.append(vector)
    
    return(vectorList)



def avgVector(vectorList):
    "Retourne le vecteur moyen à partir d'une liste de vecteurs de meme taille"
    
    nbOfvector = len(vectorList)
    vectorSum = vectorList[0]
    
    for i in range(1, nbOfvector):
        vectorSum = vectorSum + vectorList[i]
        
    return((1/nbOfvector)*vectorSum)
            

def covMatrix(vectorList, avgVector):
    "Retourne la matrice de covariance"
    nbOfvector = len(vectorList)
    matrixSum = np.dot(vectorList[0]-avgVector, np.transpose((vectorList[0]-avgVector)))
     
    for i in range(1, nbOfvector):
        matrixSum = matrixSum + np.dot(vectorList[i]-avgVector, np.transpose((vectorList[i]-avgVector))) 
     
    return((1/nbOfvector)*matrixSum)


    
def eigenVector(matrix):
    "Retourne la liste des vecteurs propres d'une matrice triés par ordre decroissant de valeurs propre"
    evalue, evect = np.linalg.eig(matrix)
    return(evect)

def eigenValue(matrix):
    "Retourne la liste des valeurs propres d'une matrice triés par ordre decroissant"
    evalue, evect = np.linalg.eig(matrix)
    return(evalue)

  
"""def eigenVector(matrix):
    "Retourne la liste des vecteurs propres d'une matrice triés par ordre decroissant de valeurs propre"
    evalue, evect = np.linalg.eig(matrix)
    evalue, evect = zip(*sorted(zip(evalue, evect)))
    
    return(evect)

def eigenValue(matrix):
    "Retourne la liste des valeurs propres d'une matrice triés par ordre decroissant"
    evalue, evect = np.linalg.eig(matrix)  
    evalue, evect = zip(*sorted(zip(evalue, evect)))
    return(evalue)
"""

def computeKLCoefs(vectorList, eigenVector):
    coefList = []
    nbOfVector = len(vectorList)
    nbOfEigenVector = len(eigenVector)
    eigenVector = np.transpose(eigenVector); 
    

    for i in range(0, nbOfVector):
        ci = []
        for j in range(0, nbOfEigenVector):
            c = np.dot(np.transpose(vectorList[i]),eigenVector[j])
            ci.append(c[0])
        
        coefList.append(ci)
        
    return(coefList)
    


def reBuildMatrix(coefList, eigenVector, pictureHeight, pictureWidth, keptCoefNb):
    "Reconstruit une matrice a partir des coefiscients de KL et des vecteurs propres de la matrice"
    nbOfBlock = len(coefList)
    vrecList = []
    
    eigenVector = np.transpose(eigenVector)
    
    for i in range(0, nbOfBlock):
        vrec = np.zeros((len(eigenVector), 1))
        
#        for j in range(0, len(coefList[i])):
        for j in range(0,keptCoefNb):
            
            for k in range(0, len(eigenVector[j])):
                
                vrec[k][0] = vrec[k][0] + coefList[i][j]*eigenVector[j][k]
               
        
        vrecList.append(vrec)

    matrix = np.zeros((pictureHeight, pictureWidth))
    
    blockSize = int(math.sqrt(len(vrecList[0])))
    
    blockPerLigne = int(pictureWidth/blockSize)
    blockPerCol = int(pictureHeight/blockSize)
    
    index = 0
    
    for i in range(0, blockPerCol):
        for j in range (0, blockPerLigne):
            
            for k in range(0, blockSize):
                for l in range(0, blockSize):
                    
                   
                    matrix[i*blockSize + k][j*blockSize + l] = vrecList[index][k*blockSize+l]
                    
            index = index + 1
    
    return(matrix)


"""
picturePath = 'lena.png'
blockSize = 8
matriceA = adaptedMatrix(fromPicToMatrix(picturePath), blockSize)
liste = vectorise(matriceA, blockSize)
matrice = covMatrix(liste, avgVector(liste))
vecteursPropres = eigenVector(matrice)
valeursPropres = eigenValue(matrice)
coefListe = computeKLCoefs(liste, vecteursPropres)
pictureHeight = matriceA.shape[0]
pictureWidth = matriceA.shape[1]
newMatrix = reBuildMatrix(coefListe, vecteursPropres , pictureHeight, pictureWidth, 64)
fromMatrixToPic(newMatrix)
plt.plot(liste[0])
plt.show()
plt.plot(coefListe[0])
plt.show()
"""

def computeEigenValueForFile(folder_name, file_name, blocksize):
    "calcule la matrice de vecteurs propres pour un fichier plaçé dans une dossier"
    picturePath = folder_name + "/" + file_name
    matriceA = adaptedMatrix(fromPicToMatrix(picturePath), blocksize)
    liste = vectorise(matriceA, blocksize)
    matrice = covMatrix(liste, avgVector(liste))
    vecteursPropres = eigenVector(matrice)
    
    return (vecteursPropres)
    
    
    
def averageEigenValueForFolder(folder_name, blocksize):
    "calcule la moyenne des matrice de vecteurs propres pour les fichiers plaçés dans une dossier"
    files = os.listdir(folder_name)
    SumEigen = computeEigenValueForFile(folder_name, files[0], blocksize)
    for i in range(1, len(files)):
        SumEigen = SumEigen + computeEigenValueForFile(folder_name, files[i], blocksize)
    
    return ((1/len(files))*SumEigen)
    
 
def applicationAverage(picture_path, averagePropre, blockSize, keptCoefNb):
    "utilise la moyenne de vecteurs propres pour reconstruire l'image"
    matriceA = adaptedMatrix(fromPicToMatrix(picture_path), blockSize)
    liste = vectorise(matriceA, blockSize)
    coefListe = computeKLCoefs(liste, averagePropre)
    pictureHeight = matriceA.shape[0]
    pictureWidth = matriceA.shape[1]
    newMatrix = reBuildMatrix(coefListe, averagePropre , pictureHeight, pictureWidth, keptCoefNb)
    fromMatrixToPic(newMatrix)
    
    
    return 0


blockSize=16
averagePropre = averageEigenValueForFolder("ImgFolder", blockSize)
applicationAverage("lena.png", averagePropre, blockSize, 15)